angular.module('smartPaperApp.directives', [])
    .directive('hideMenus', ['$log', '$window', function($log, $window) {
            return {
                scope:{
                    rightMenu: '=',
                    leftMenu: '='
                },
                link: function(scope, element, attributes) {

                    
                    scope.closeAll = function(){
                        scope.rightMenu = false;
                        scope.leftMenu = false;
                    }
                
                    $(element).on('affix-top.bs.affix', function () {
                        scope.$apply(function(){
                            $log.info("affix-top.bs.affix directive");
                            scope.closeAll();
                        });
                    });
                    angular.element($window).on('click', function(){
                        scope.$apply(function(){
                            //scope.closeAll();
                        });
                    });
                }
            };
    }])
    .directive('twitterModal', ['$log', '$window', function($log, $window) {
        return {
            link: function (scope, element) {
                $(element).on('click', function() {
                    $window.open(scope.twitterModel.getLink(), 'Share twitter', 'width=460,height=460,left=300');
                });
            }
        }
    }])
    .directive('repeatDone', function() {
        return function(scope, element, attrs) {
            if (scope.$last) { // all are rendered
                scope.$eval(attrs.repeatDone);
            }
        }
    });
