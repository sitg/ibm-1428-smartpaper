function MainController($scope, $log, $timeout, highlights, contentTable, $interval) {
    $scope.highlights = highlights;
    $scope.contentTable = contentTable;
    $scope.Sections = [];
    $scope.sharePopoverOpened = false;
    $scope.searchWindowOpened = false;
    $scope.leftMenuOpened = false;
    $scope.rightMenuOpened = false;
    $scope.subMenuHighlightOpened = -1;
    $scope.searchText = "";
    $scope.textRange = null;
    $scope.selectedText = "";

    $scope.shareSelectedPopoverModel = {
        opened: false,
        top: 0,
        left: 0
    };

    $scope.emailModel = {
        subject: "IBM ExperienceOne Seventh Annual Online Retail Holiday Readiness Report",
        emailAddress: "info@somnio.com",
        bodyContent: "",
        getHREF: function() {
            if($scope.selectedText==""){
            this.subject ="Enhance Your 360-degree View of the Customer - An IBM Interactive White Paper";
            }else {
            this.subject = $scope.selectedText;
            }
            this.bodyContent = "http://ibm.com " + $scope.selectedText;
            return "mailto:" + this.emailAddress + "?subject=" + this.subject + "&body=" + this.bodyContent;
        }
    }

    $scope.twitterModel = {
        target: "_blank",
        baseUrl: "https://twitter.com/intent/tweet",
        text: "",
        getLink: function() {
            if($scope.selectedText==""){
                 this.text="Does your big data hold the secret to a better customer experience? Find out: http://ibm.com";
            }else {
            this.text = $scope.selectedText;
            }
            return this.baseUrl + "?text=" + this.text;
        }
    }

    $scope.shareSelectedTextOpened = false;

    $scope.init = function () {
        $scope.Sections = $scope.getSections();
    };

    $scope.stopPropagation = function(event){
        event.stopPropagation();
    }

    $scope.onMouseUp = function(event) {
        if (!$.jagent.isIPad() && !$.jagent.isIPhone()){
            $scope.selectedText = $scope.getSelectedText();

            $scope.shareSelectedPopoverModel.top = event.pageY - 70 > 0 ? event.pageY - 70 : 0;
            $scope.shareSelectedPopoverModel.left = event.pageX - 40 > 0 ?  event.pageX - 40: 0;

            $scope.shareSelectedPopoverModel.opened = $scope.selectedText.length > 0;
        }
    }

    var timer = $interval(function() {
        var selectedText = $scope.getSelectedText();
        if (selectedText){
            $scope.selectedText = selectedText;
        }
    }, 150);

    $scope.resetSelection = function() {
        $scope.selectedText = "";
    }

    $scope.openCloseSearchWindow = function(event) {
        $scope.searchWindowOpened = !$scope.searchWindowOpened;
        $scope.sharePopoverOpened = false;
        $scope.leftMenuOpened = false;
        $scope.rightMenuOpened = false;
        $scope.stopPropagation(event);
    }


    $scope.openCloseSharePopover = function(event) {
        $scope.sharePopoverOpened = !$scope.sharePopoverOpened;
        $scope.searchWindowOpened = false;
        $scope.leftMenuOpened = false;
        $scope.rightMenuOpened = false;
        $scope.stopPropagation(event);
    }

    $scope.openCloseHighlightMenuClick = function(event){
        $scope.rightMenuOpened = !$scope.rightMenuOpened;
        $scope.searchWindowOpened = false;
        $scope.sharePopoverOpened = false;
        $scope.leftMenuOpened = false;
        $scope.stopPropagation(event);
    };

    $scope.openCloseLeftMenuOptionClick = function(event){
        $scope.leftMenuOpened = !$scope.leftMenuOpened;
        $scope.searchWindowOpened = false;
        $scope.sharePopoverOpened = false;
        $scope.rightMenuOpened = false;
        $scope.stopPropagation(event);
    };

    $scope.closeRightMenuOptionClick = function(event){
        $scope.rightMenuOpened = false;
        $scope.stopPropagation(event);
    };


    $scope.cutText = function(text){
        var maxNumberOfCharacters = 90;

        return text.length >= maxNumberOfCharacters ? text.substring(0, maxNumberOfCharacters) + '...' : text;
    };

    $scope.getSections = function () {
        var asArray = [];

        angular.forEach($scope.contentTable, function (content, i) {
            content.isMainSection = true;
            asArray.push(content);
            angular.forEach(content.sections, function(section, j) {
                section.isMainSection = false;
                asArray.push(section);
            });
        });

        return asArray;
    };

    $scope.getUrl = function (section) {
        return "partials/" + section.id + ".html";
    }

    $scope.getSelectedText = function() {
        var html = "";
        if (typeof window.getSelection != "undefined") {
            var sel = window.getSelection();
            if (sel.rangeCount) {
                var container = document.createElement("div");
                for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                    container.appendChild(sel.getRangeAt(i).cloneContents());
                }
                html = container.innerHTML;
            }
        } else if (typeof document.selection != "undefined") {
            if (document.selection.type == "Text") {
                html = document.selection.createRange().htmlText;
            }
        }

        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;

        return tmp.textContent || tmp.innerText || "";
    }

    $scope.buildBreadcrumb = function(sections){
        $timeout(function(){
            var breadCrumbWidth = angular.element(".navbar-crumbs").width();

            var tabWidth = ((breadCrumbWidth/sections)*100)/breadCrumbWidth;

            angular.element("#dynamicNavBar .nav.navbar-nav.navbar-justified>li").css('width', tabWidth + "%");
        });
    };

    $scope.init();

}

function AcoordionController($rootScope, $scope, $window, $log) {

    $scope.activeSections = [];
    $scope.disabled = false;

    $scope.getIndex = function(id) {
        return $scope.activeSections.indexOf(id);
    }

    $scope.opened = function (id) {
        return $scope.disabled || ($scope.getIndex(id) > -1);
    }

    $scope.openSection = function(id) {
        var index = $scope.getIndex(id);
        if (index > - 1) { // if is Open
            $scope.activeSections.splice(index, 1);
        } else {
            $scope.activeSections.push(id);
        }
    }

    $scope.onWidthChange = function() {
        if ($window.matchMedia){
            var mql = $window.matchMedia("(min-width: 768px)");
            if (mql.matches) {
                $scope.disabled = true;
            } else {
                $scope.disabled = false;
            }
            mql.addListener(handleWidthChange);
        }
        $scope.openSection('introduction');
        //todo: check no supported object in devices
    }

    var handleWidthChange = function(mql) {
        var $scope = angular.element('.sections').scope();

        if (mql.matches) {
            $scope.$apply(function () {
                $scope.disabled = true;
            });
        } else {
            $scope.$apply(function () {
                $scope.disabled = false;
            });
        }
    }

    $scope.init = function() {
        $scope.onWidthChange();
    }

    $scope.init();
}