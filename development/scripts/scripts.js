
$( function () {

	$('#btnInfo').popover();
        
        $(document).ready(function(){
            
        });

	var previousHeight = $("#cover").height() + $("#contentTable").height() - 110;
	$("#dynamicNavBar").affix({
		offset: {
			top: previousHeight
		}
	});

	$('.nav-controls').on('activate.bs.scrollspy', updateCrumbs);
    var locationPath = filterPath(location.pathname);

	updateCrumbs();

	// Smooth Scrolling
	$('a[href*=#]:not([data-toggle=collapse]):not(.internal-anchor)').click(function() {
        var thisPath = filterPath(this.pathname) || locationPath;
	if (locationPath === thisPath && (location.hostname === this.hostname || !this.hostname)) {
	  var target = $(this.hash);
	  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

	  if (target.hasClass('.internal-anchor')) {
	  	return;
	  }

	  if (!target.hasClass('.internal-anchor') && this.hash.length) {
	  	  var secId = $(this).attr("data-secid");
	  	  if (secId && secId.length) {
			  var $scope = angular.element('#' + secId).scope();
			  if ($scope.getIndex(secId) === -1) {
				  $scope.openSection(secId);
				  $scope.$apply();	  		  	  				  	
			  }

	  	  } else {
			  var $scope = angular.element(this.hash).scope();
			  if ($scope && $scope.openSection) {
			  	if ($scope.getIndex(this.hash.slice(1)) === -1) {
					$scope.openSection(this.hash.slice(1));
					$scope.$apply();	  				  				  		
			  	}
			  }
	  	  }
	  }

	  if (target.length) {
	    $('html,body').animate({
	      scrollTop: target.offset().top-100
	    }, 1000);
	    return false;
	  }
	}
	});

});

function filterPath(string) {
    return string
        .replace(/^\//,'')
        .replace(/(index|default).[a-zA-Z]{3,4}$/,'')
        .replace(/\/$/,'');
}

function updateCrumbs() {
	var $current = $(".navbar-crumbs .active");
	$current.css("background-color", "");
	$current.find(".fa").css("color", "");

	var $nextAll = $current.nextAll();
	$nextAll.css("background-color", "");
	$nextAll.find(".fa").css("color", "");

	var $prevAll = $current.prevAll();
    $prevAll.css("background-color", "#B9EEFC");
	$prevAll.find(".fa").css("color", "#00a6a0");		

	$current = $(".side-menu.left-menu li.active");
	$current.not(".section").css("background-color", "#E8E8E8");

	$nextAll = $current.nextAll();
	$nextAll.not(".section").css("background-color", "");

	$prevAll = $current.prevAll().not(".section");
	$prevAll.not(".section").css("background-color", "#E8E8E8");	
}


var textRange=null;
function findNext(text) {
    if ($.jagent.isIPad() || $.jagent.isIPhone()){
        return findNextOnDevice(text);
    }
    else if (window.find) {
        return window.find(text);
    }
    else if (document.body.createTextRange) {
        if (textRange === null) {
            textRange = document.body.createTextRange();
        }
        if (textRange.findText(text)) {
            textRange.select();
            textRange.scrollIntoView();
            textRange.moveStart("word");
            return true;
        } else {
            textRange = null;
        }
    }
    return false;
}

var  cont = 0;
var searchText = "";
function findNextOnDevice(text){
    if (searchText !== text){
        cont = 0;
        searchText = text;
    }
    $('.sections').removeHighlight();
    $('.sections').highlight(text);

    var foundElements = $('span.highlight-search');
    if (foundElements.length === 0 || foundElements.length  === cont ){
        cont = 0;
        searchText = text;
        return false;
    }
    var elem = $('span.highlight-search:eq('+ cont +')').css("background-color","#00a6a0");
    var offset = 100;
    if($.jagent.isIPhone()){
        openSection(elem);
        offset = 60;
    }
    $('body').animate({scrollTop: elem.offset().top - offset}, 500);
    cont ++;

    return true;
}

function openSection(ele){
    $('.sections .accordion-section').each(function(index ) {
        var self = this;
        if ($(self).find(ele).length > 0){
            var $scope = angular.element(self).scope();
            var sectionId = $(".internal-anchor", self).attr('id');

            if ($scope.getIndex(sectionId) === -1){
                $scope.$apply( function () {
                    $scope.openSection(sectionId);
                });
                return;
            }
        }
    });
}

function toggleSearchWindow() {
	var $scope = angular.element("body").scope();
	if ($scope) {
		$scope.$apply( function () {
			$scope.openCloseSearchWindow();
		});
	}
}






