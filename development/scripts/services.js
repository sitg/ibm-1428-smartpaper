angular.module("smartPaperApp.services", [])
        .factory('highlights', [function () {
                return {
                    title: "Select a topic below see all related sections within the white paper.",
                    options: [
                        {
                            'topic': 'The Benefits of Big Data',
                            'href': 'benefits_of_big_data',
                            'width': '35',
                            'subtopics': [
                                {
                                    'topic': 'Personalized Customer Experiences',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight01',
                                    'secId': 'introduction'
                                },
                                {
                                    'topic': 'Increased Customer Loyalty',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight02',
                                    'secId': 'understand_customers'
                                },
                                {
                                    'topic': 'Having A Complete Picture Of Your Customer',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight03',
                                    'secId': 'understand_customers'
                                }
                            ]
                        },
                        {
                            'topic': 'See The 360-Degree View In Action',
                            'href': '360_degree',
                            'width': '60', /*Width is in percent. Max width must be 100%. Min width must be 20%*/
                            'subtopics': [
                                {
                                    'topic': 'Reducing Costs and Simplifying Workflows',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight04',
                                    'secId': 'introduction'
                                },
                                {
                                    'topic': 'Improving Customer Satisfaction',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight05',
                                    'secId': 'understand_customers'
                                },
                                {
                                    'topic': 'Enhancing Service Delivery',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight06',
                                    'secId': 'understand_customers'
                                },
                                {
                                    'topic': 'Improving Targeted Marketing',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight07',
                                    'secId': 'build_an_enhanced'
                                }
                            ]
                        },
                        {
                            'topic': 'Keys To Success',
                            'href': 'keys_to_success',
                            'width': '32', /*Width is in percent. Max width must be 100%. Min width must be 20%*/
                            'subtopics': [
                                {
                                    'topic': 'Establishing An Enhanced 360-Degree View',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight08',
                                    'secId': 'build_an_enhanced'
                                },
                                {
                                    'topic': 'Finding The Right Solution',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight09',
                                    'secId': 'expand_your_view'
                                },
                                {
                                    'topic': 'The Cornerstone Of Big Data and Analytics',
                                    'preview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales dapibus pulvinar. Suspendisse ut enim dictum, quisque placerat libero dolor, dui scelerisque',
                                    'href': 'highlight10',
                                    'secId': 'expand_your_view'
                                }
                            ]
                        }
                    ]
                };
            }])
        .factory('contentTable', [function () {
                return [
                    {
                        'title': 'Introduction',
                        'id': 'introduction',
                        'isMain': false,
                        'sections': [
                            {
                                'title': 'Understand Customer Better',
                                'id': 'understand_customers',
                                'isMain': true
                            },
                            {
                                'title': 'Build an enhanced 360-degree view',
                                'id': 'build_an_enhanced',
                                'isMain': true
                            },
                            {
                                'title': 'Expand your view with IBM solutions',
                                'id': 'expand_your_view',
                                'isMain': true
                            }
                        ]
                    }
                    ,
                    {
                        'title': 'Resources',
                        'id': 'more_information',
                        'isMain': false,
                        'isExcluded':true,
                        'sections': [
                            {
                                'title': 'Sources',
                                'id': 'sources',
                                'isMain': false,
                                'isExcluded': true
                            }
                        ]
                    }
                ];
            }]);